package com.layanacomputindo.sun7ara.Activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.layanacomputindo.sun7ara.Fragment.FragmentDashboard;
import com.layanacomputindo.sun7ara.Fragment.FragmentLevelCareer;
import com.layanacomputindo.sun7ara.R;

public class LevelCareerActivity extends AppCompatActivity {

    static Fragment mFragment = null;
    static Class mClass;
    static FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_career);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Level 3: Be The Person In Your Team");

        mFragmentManager = getSupportFragmentManager();
        mClass = FragmentLevelCareer.class;
        try {
            mFragment = (Fragment) mClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mFragmentManager.beginTransaction().replace(R.id.nav_contentframe, mFragment).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
