package com.layanacomputindo.sun7ara.Activity;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.layanacomputindo.sun7ara.Fragment.FragmentAchievement;
import com.layanacomputindo.sun7ara.Fragment.FragmentCalendar;
import com.layanacomputindo.sun7ara.Fragment.FragmentCareer;
import com.layanacomputindo.sun7ara.Fragment.FragmentCatalog;
import com.layanacomputindo.sun7ara.Fragment.FragmentDashboard;
import com.layanacomputindo.sun7ara.Fragment.FragmentNews;
import com.layanacomputindo.sun7ara.R;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;
    private NavigationView mNavigationView;

    static Fragment mFragment = null;
    static Class mClass;
    static FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer);
        mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawerLayout, mToolbar, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView= (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mFragmentManager = getSupportFragmentManager();
        mClass = FragmentDashboard.class;
        getSupportActionBar().setTitle("Dashboard");
        try {
            mFragment = (Fragment) mClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mFragmentManager.beginTransaction().replace(R.id.nav_contentframe, mFragment).commit();

    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            mClass = FragmentDashboard.class;
            getSupportActionBar().setTitle("Dashboard");
        } else if (id == R.id.nav_carrier) {
            mClass = FragmentCareer.class;
            getSupportActionBar().setTitle("Career");
        } else if (id == R.id.nav_chat) {

        } else if (id == R.id.nav_news) {
            mClass = FragmentNews.class;
            getSupportActionBar().setTitle("News");
        } else if (id == R.id.nav_calendar) {
            mClass = FragmentCalendar.class;
            getSupportActionBar().setTitle("Calendar");
        } else if (id == R.id.nav_catalog) {
            mClass = FragmentCatalog.class;
            getSupportActionBar().setTitle("Product Catalog");
        } else if (id == R.id.nav_achievement) {
            mClass = FragmentAchievement.class;
            getSupportActionBar().setTitle("Achievement & Rewards");
        } else if (id == R.id.nav_archive) {
            mClass = FragmentAchievement.class;
            getSupportActionBar().setTitle("Archive");
        } else if (id == R.id.nav_mapping) {
            mClass = FragmentAchievement.class;
            getSupportActionBar().setTitle("Mapping Member");
        } else if (id == R.id.nav_leader) {
            mClass = FragmentAchievement.class;
            getSupportActionBar().setTitle("Profile Leader");
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);

        try {
            mFragment = (Fragment) mClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mFragmentManager.beginTransaction().replace(R.id.nav_contentframe, mFragment).commit();

        return true;
    }
}
