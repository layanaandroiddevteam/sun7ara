package com.layanacomputindo.sun7ara.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.layanacomputindo.sun7ara.Model.Area;
import com.layanacomputindo.sun7ara.R;
import com.layanacomputindo.sun7ara.Rest.RestClient;

import java.util.ArrayList;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class RegisterActivity extends AppCompatActivity {

    private CompositeSubscription compositeSubscription;
    private Observable<ArrayList<Area>> call;
    private String TAG = "Area";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        compositeSubscription = new CompositeSubscription();

        compositeSubscription.clear();
        RestClient.WellhosService service = RestClient.getClient();
        call = service.getArea();

        Subscription subscription = call
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(data -> {
                    Log.d(TAG, new Gson().toJson(data));
                }, error -> {
                    Log.e(TAG, "error " + error.getLocalizedMessage());
                });
        compositeSubscription.add(subscription);

    }
}
