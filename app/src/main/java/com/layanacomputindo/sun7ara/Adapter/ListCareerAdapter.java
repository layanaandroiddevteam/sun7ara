package com.layanacomputindo.sun7ara.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.layanacomputindo.sun7ara.Activity.LevelCareerActivity;
import com.layanacomputindo.sun7ara.Model.Career;
import com.layanacomputindo.sun7ara.R;

import java.util.ArrayList;
import java.util.List;


public class ListCareerAdapter extends RecyclerView.Adapter<ListCareerAdapter.ViewHolder> {

    List<Career> mList;
    private Context mContext;

    public ListCareerAdapter(Context context, ArrayList<Career> list) {
        super();

        mContext = context;

        mList = list;
        for(int i = 0 ; i < 3 ; i++){
            Career dc = new Career();
            dc.setTitle("Level 3: Be The Person In Your Team");
            dc.setDesc("Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet");
            mList.add(dc);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.career_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Career model = mList.get(i);
        viewHolder.txtTitle.setText(model.getTitle());
        viewHolder.txtDesc.setText(model.getDesc());

        viewHolder.currentItem = mList.get(i);

    }



    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtTitle, txtDesc;

        public Career currentItem;

        public ImageView imgPhoto;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView)itemView.findViewById(R.id.txtTitle);
            txtDesc = (TextView)itemView.findViewById(R.id.txtDesc);
            imgPhoto = (ImageView)itemView.findViewById(R.id.imgPhoto);

            itemView.setOnClickListener(view -> {
                Intent i = new Intent(mContext, LevelCareerActivity.class);
                mContext.startActivity(i);
            });
        }

    }
}