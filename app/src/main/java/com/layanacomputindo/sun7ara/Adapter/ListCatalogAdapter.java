package com.layanacomputindo.sun7ara.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.layanacomputindo.sun7ara.Activity.DetilAchievementActivity;
import com.layanacomputindo.sun7ara.Activity.DetilCatalogActivity;
import com.layanacomputindo.sun7ara.Model.Achievement;
import com.layanacomputindo.sun7ara.Model.Catalog;
import com.layanacomputindo.sun7ara.R;

import java.util.ArrayList;
import java.util.List;


public class ListCatalogAdapter extends RecyclerView.Adapter<ListCatalogAdapter.ViewHolder> {

    List<Catalog> mList;
    private Context mContext;

    public ListCatalogAdapter(Context context, ArrayList<Catalog> list) {
        super();

        mContext = context;

        mList = list;
        for(int i = 0 ; i < 3 ; i++){
            Catalog dc = new Catalog();
            dc.setTitle("Meeting Mingguan");
            dc.setDesc("Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet");
            mList.add(dc);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.achievement_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Catalog model = mList.get(i);
        viewHolder.txtTitle.setText(model.getTitle());
        viewHolder.txtDesc.setText(model.getDesc());

        viewHolder.currentItem = mList.get(i);

    }



    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtTitle, txtDesc;

        public Catalog currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView)itemView.findViewById(R.id.txtTitle);
            txtDesc = (TextView)itemView.findViewById(R.id.txtDesc);

            itemView.setOnClickListener(view -> {
                Intent i = new Intent(mContext, DetilCatalogActivity.class);
                mContext.startActivity(i);
            });
        }

    }
}