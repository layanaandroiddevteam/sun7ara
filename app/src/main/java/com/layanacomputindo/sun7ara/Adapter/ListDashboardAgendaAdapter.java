package com.layanacomputindo.sun7ara.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.layanacomputindo.sun7ara.Activity.DetilAgendaActivity;
import com.layanacomputindo.sun7ara.Model.Agenda;
import com.layanacomputindo.sun7ara.Model.News;
import com.layanacomputindo.sun7ara.R;

import java.util.ArrayList;
import java.util.List;


public class ListDashboardAgendaAdapter extends RecyclerView.Adapter<ListDashboardAgendaAdapter.ViewHolder> {

    List<Agenda> mList;
    private Context mContext;

    public ListDashboardAgendaAdapter(Context context, ArrayList<Agenda> list) {
        super();

        mContext = context;

        mList = list;
        for(int i = 0 ; i < 3 ; i++){
            Agenda dc = new Agenda();
            dc.setTitle("Meeting Mingguan");
            dc.setDesc("Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet");
            dc.setTime("at 14:0016:30");
            dc.setPlace("@Office");
            mList.add(dc);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dashboardagenda_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Agenda model = mList.get(i);
        viewHolder.txtTitle.setText(model.getTitle());
        viewHolder.txtDesc.setText(model.getDesc());
        viewHolder.txtTime.setText(model.getTime());
        viewHolder.txtPlace.setText(model.getPlace());

        viewHolder.currentItem = mList.get(i);

    }



    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtTitle, txtDesc, txtTime, txtPlace;

        public Agenda currentItem;
        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView)itemView.findViewById(R.id.txtTitle);
            txtDesc = (TextView)itemView.findViewById(R.id.txtDesc);
            txtTime = (TextView)itemView.findViewById(R.id.txtTime);
            txtPlace = (TextView)itemView.findViewById(R.id.txtPlace);

            itemView.setOnClickListener(view -> {
                Intent i = new Intent(mContext, DetilAgendaActivity.class);
                mContext.startActivity(i);
            });
        }

    }
}