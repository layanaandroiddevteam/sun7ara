package com.layanacomputindo.sun7ara.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.layanacomputindo.sun7ara.Activity.DetilCatalogActivity;
import com.layanacomputindo.sun7ara.Activity.DetilNewsActivity;
import com.layanacomputindo.sun7ara.Model.Career;
import com.layanacomputindo.sun7ara.Model.News;
import com.layanacomputindo.sun7ara.R;

import java.util.ArrayList;
import java.util.List;


public class ListNewsAdapter extends RecyclerView.Adapter<ListNewsAdapter.ViewHolder> {

    List<News> mList;
    private Context mContext;

    public ListNewsAdapter(Context context, ArrayList<News> list) {
        super();

        mContext = context;

        mList = list;
        for(int i = 0 ; i < 3 ; i++){
            News dc = new News();
            dc.setAuthor("Amir Soraya");
            dc.setJabatan("Cabang Yogyakarta");
            dc.setTime("3 minutes ago");
            dc.setDesc("Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet");
            mList.add(dc);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.news_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        News model = mList.get(i);
        viewHolder.txtAuthor.setText(model.getAuthor());
        viewHolder.txtJabatan.setText(model.getJabatan());
        viewHolder.txtTime.setText(model.getTime());
        viewHolder.txtDesc.setText(model.getDesc());

        viewHolder.currentItem = mList.get(i);

    }



    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtTime, txtAuthor, txtJabatan, txtDesc;

        public News currentItem;

        public ImageView imgPhoto;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTime = (TextView)itemView.findViewById(R.id.txtTime);
            txtAuthor = (TextView)itemView.findViewById(R.id.txtAuthor);
            txtJabatan = (TextView)itemView.findViewById(R.id.txtJabatan);
            txtDesc = (TextView)itemView.findViewById(R.id.txtDesc);
            imgPhoto = (ImageView)itemView.findViewById(R.id.imgPhoto);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, DetilNewsActivity.class);
                    mContext.startActivity(i);
                }
            });
        }

    }
}