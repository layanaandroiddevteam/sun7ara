package com.layanacomputindo.sun7ara.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.layanacomputindo.sun7ara.Adapter.ListCareerAdapter;
import com.layanacomputindo.sun7ara.Adapter.ListDashboardCareerAdapter;
import com.layanacomputindo.sun7ara.Model.Career;
import com.layanacomputindo.sun7ara.R;

import java.util.ArrayList;

public class FragmentCareer extends Fragment {

    public ArrayList<Career> mList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_recycleview_amber, container, false);

        RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        ListCareerAdapter mAdapter = new ListCareerAdapter(v.getContext(), mList);
        mRecyclerView.setAdapter(mAdapter);

        return v;
    }

}