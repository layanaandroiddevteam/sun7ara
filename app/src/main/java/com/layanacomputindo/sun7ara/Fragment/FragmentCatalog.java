package com.layanacomputindo.sun7ara.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.layanacomputindo.sun7ara.Adapter.ListAchievementAdapter;
import com.layanacomputindo.sun7ara.Adapter.ListCatalogAdapter;
import com.layanacomputindo.sun7ara.Model.Achievement;
import com.layanacomputindo.sun7ara.Model.Catalog;
import com.layanacomputindo.sun7ara.R;
import com.layanacomputindo.sun7ara.Util.PaddingList;

import java.util.ArrayList;

public class FragmentCatalog extends Fragment {

    public ArrayList<Catalog> mList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new PaddingList(20));

        ListCatalogAdapter mAdapter = new ListCatalogAdapter(v.getContext(), mList);
        mRecyclerView.setAdapter(mAdapter);

        return v;
    }

}