package com.layanacomputindo.sun7ara.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.layanacomputindo.sun7ara.Adapter.ListCareerAdapter;
import com.layanacomputindo.sun7ara.Adapter.ListNewsAdapter;
import com.layanacomputindo.sun7ara.Model.Career;
import com.layanacomputindo.sun7ara.Model.News;
import com.layanacomputindo.sun7ara.R;

import java.util.ArrayList;

public class FragmentPublicNews extends Fragment {

    public ArrayList<News> mList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        ListNewsAdapter mAdapter = new ListNewsAdapter(v.getContext(), mList);
        mRecyclerView.setAdapter(mAdapter);

        return v;
    }

}