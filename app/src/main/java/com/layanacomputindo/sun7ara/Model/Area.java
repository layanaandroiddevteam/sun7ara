package com.layanacomputindo.sun7ara.Model;

/**
 * Created by Yoshua Andrean on 08/07/2016.
 */
public class Area {

    private String area_id;
    private String area_code;
    private String area_title;
    private String area_content;
    private String area_status;


    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getArea_code() {
        return area_code;
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    public String getArea_title() {
        return area_title;
    }

    public void setArea_title(String area_title) {
        this.area_title = area_title;
    }

    public String getArea_content() {
        return area_content;
    }

    public void setArea_content(String area_content) {
        this.area_content = area_content;
    }

    public String getArea_status() {
        return area_status;
    }

    public void setArea_status(String area_status) {
        this.area_status = area_status;
    }
}
