package com.layanacomputindo.sun7ara.Rest;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.layanacomputindo.sun7ara.Model.Area;

import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import rx.Observable;

public class RestClient {

    private static WellhosService wellhosService ;
    private static String baseUrl = "http://asuransi.kartuvirtual.com/";

    private static Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();

    private static OkHttpClient.Builder okhttpBuilder = new OkHttpClient.Builder();

    public static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

    public static Retrofit retrofit;

    public static WellhosService getClient() {
        //okhttpBuilder.interceptors().add(authentication);
        OkHttpClient client = okhttpBuilder.build();

        retrofit = retrofitBuilder.client(client).build();

        wellhosService = retrofit.create(WellhosService.class);
        return wellhosService ;
    }

//    public static WellhosService getClient(Context context) {
//        sharedPreferences = context.getSharedPreferences("shared", Activity.MODE_PRIVATE);
//
//        okhttpBuilder.interceptors().add(authentication);
//        OkHttpClient client = okhttpBuilder.build();
//
//        retrofit = retrofitBuilder.client(client).build();
//
//        wellhosService = retrofit.create(WellhosService.class);
//        return wellhosService ;
//    }

//    private static final Interceptor authentication = chain -> {
//        Request originalRequest = chain.request();
//        Request authenticationRequest = originalRequest.newBuilder()
//                .header("Authorization", "Bearer " + sharedPreferences.getString("token", ""))
//                .build();
//
//        Response response = chain.proceed(authenticationRequest);
//
//        return response;
//    };

    public interface WellhosService {

        @GET("/api/asuransi/area_list")
        Observable<ArrayList<Area>> getArea();

    }
}
